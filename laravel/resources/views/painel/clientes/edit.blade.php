@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Clientes /</small> Editar Cliente</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.clientes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.clientes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
