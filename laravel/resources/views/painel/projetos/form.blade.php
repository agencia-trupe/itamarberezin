@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('projetos_categoria_id', 'Categoria') !!}
    {!! Form::select('projetos_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/projetos/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('cabecalho', 'Cabeçalho') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/projetos/'.$registro->cabecalho) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('cabecalho', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('local', 'Local') !!}
    {!! Form::text('local', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('projeto', 'Projeto') !!}
    {!! Form::text('projeto', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('area_do_terreno', 'Área do Terreno') !!}
    {!! Form::text('area_do_terreno', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('area_construida', 'Área Construída') !!}
    {!! Form::text('area_construida', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('pavimentos', 'Pavimentos') !!}
    {!! Form::text('pavimentos', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('tipo', 'Tipo') !!}
    {!! Form::text('tipo', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.projetos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
