@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('foto', 'Foto') !!}
    @if($registro->foto)
    <img src="{{ url('assets/img/institucional/'.$registro->foto) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('foto', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="row">
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1') !!}
            @if($registro->imagem_1)
            <img src="{{ url('assets/img/institucional/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2') !!}
            @if($registro->imagem_2)
            <img src="{{ url('assets/img/institucional/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem_3', 'Imagem 3') !!}
            @if($registro->imagem_3)
            <img src="{{ url('assets/img/institucional/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem_4', 'Imagem 4') !!}
            @if($registro->imagem_4)
            <img src="{{ url('assets/img/institucional/'.$registro->imagem_4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_4', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem_5', 'Imagem 5') !!}
            @if($registro->imagem_5)
            <img src="{{ url('assets/img/institucional/'.$registro->imagem_5) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_5', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem_6', 'Imagem 6') !!}
            @if($registro->imagem_6)
            <img src="{{ url('assets/img/institucional/'.$registro->imagem_6) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_6', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem_7', 'Imagem 7') !!}
            @if($registro->imagem_7)
            <img src="{{ url('assets/img/institucional/'.$registro->imagem_7) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_7', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem_8', 'Imagem 8') !!}
            @if($registro->imagem_8)
            <img src="{{ url('assets/img/institucional/'.$registro->imagem_8) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_8', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
