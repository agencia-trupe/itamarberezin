@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Prêmios /</small> Adicionar Prêmio</h2>
    </legend>

    {!! Form::open(['route' => 'painel.premios.store', 'files' => true]) !!}

        @include('painel.premios.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
