@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Prêmios /</small> Editar Prêmio</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.premios.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.premios.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
