@extends('frontend.common.template')

@section('absoluteContent')

    <div class="projetos-cabecalho" style="background-image: url({{ asset('assets/img/projetos/'.$projeto->cabecalho) }})"></div>

@endsection


@section('content')

    <?php $half = ceil($projeto->imagens->count() / 2); ?>

    <div class="content projetos-show">
        <div class="desktop">
            <div class="left">
                <h2>{{ $projeto->titulo }}</h2>
                @if(count($projeto->imagens) && array_key_exists(0, $projeto->imagens->chunk($half)->toArray()))
                <div class="imagens">
                    @foreach($projeto->imagens->chunk($half)[0] as $imagem)
                    <img src="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" alt="">
                    @endforeach
                </div>
                @endif
            </div>
            <div class="right">
                <div class="detalhes">
                    @foreach([
                        'local'           => 'Local',
                        'projeto'         => 'Projeto',
                        'area_do_terreno' => 'Área do Terreno',
                        'area_construida' => 'Área Construída',
                        'pavimentos'      => 'Pavimentos',
                        'tipo'            => 'Tipo'
                    ] as $field => $title)
                        @if($projeto->{$field})
                        <div class="row">
                            <span>{{ $title }}:</span>
                            <span>{{ $projeto->{$field} }}</span>
                        </div>
                        @endif
                    @endforeach
                </div>
                @if(count($projeto->imagens) && array_key_exists(1, $projeto->imagens->chunk($half)->toArray()))
                <div class="imagens">
                    @foreach($projeto->imagens->chunk($half)[1] as $imagem)
                    <img src="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" alt="">
                    @endforeach
                </div>
                @endif
            </div>
        </div>
        <div class="mobile">
            <h2>{{ $projeto->titulo }}</h2>
            <div class="detalhes">
                @foreach([
                    'local'           => 'Local',
                    'projeto'         => 'Projeto',
                    'area_do_terreno' => 'Área do Terreno',
                    'area_construida' => 'Área Construída',
                    'pavimentos'      => 'Pavimentos',
                    'tipo'            => 'Tipo'
                ] as $field => $title)
                    @if($projeto->{$field})
                    <div class="row">
                        <span>{{ $title }}:</span>
                        <span>{{ $projeto->{$field} }}</span>
                    </div>
                    @endif
                @endforeach
            </div>
            <div class="imagens">
                @foreach($projeto->imagens as $imagem)
                <img src="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" alt="">
                @endforeach
            </div>
        </div>
    </div>

@endsection
