@extends('frontend.common.template')

@section('content')

    <div class="content projetos-index">
        @foreach($projetos as $projeto)
        <a href="{{ route('projetos.show', [$projeto->categoria->slug, $projeto->slug]) }}">
            <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" alt="">
            <div class="overlay">
                {{ $projeto->titulo }}
            </div>
        </a>
        @endforeach
    </div>

@endsection
