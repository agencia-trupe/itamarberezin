@extends('frontend.common.template')

@section('absoluteContent')

    <div class="banners">
        @foreach($banners as $banner)
        <div class="banner" style="background-image: url({{ asset('assets/img/banners/'.$banner->imagem) }})"></div>
        @endforeach
    </div>

@endsection
