@extends('frontend.common.template')

@section('content')

    <div class="content perfil">
        <img src="{{ asset('assets/img/institucional/'.$institucional->foto) }}" alt="">
        {!! $institucional->texto !!}
    </div>

@endsection

@section('fullWidthContent')

    <div class="perfil-imagens">
        @foreach(range(1, 8) as $i)
            <img src="{{ asset('assets/img/institucional/'.$institucional->{'imagem_'.$i}) }}" alt="">
        @endforeach
    </div>

@endsection
