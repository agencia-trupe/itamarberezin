@extends('frontend.common.template')

@section('content')

    <div class="content premios">
        <p>Alguns de nossos prêmios</p>
        <div class="imagens">
            @foreach($premios as $premio)
            <div class="imagem">
                <img src="{{ asset('assets/img/premios/'.$premio->imagem) }}" alt="">
            </div>
            @endforeach
        </div>
    </div>

@endsection
