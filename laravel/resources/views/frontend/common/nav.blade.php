<a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="active" @endif>Home</a>
<a href="{{ route('perfil') }}" @if(Tools::routeIs('perfil*')) class="active" @endif>Perfil</a>
@if(Tools::routeIs('perfil*'))
<div class="sub">
    <a href="{{ route('perfil.institucional') }}" @if(Tools::routeIs(['perfil', 'perfil.institucional'])) class="active" @endif>Institucional</a>
    <a href="{{ route('perfil.premios') }}" @if(Tools::routeIs('perfil.premios')) class="active" @endif>Prêmios</a>
</div>
@endif
<a href="{{ route('projetos') }}" @if(Tools::routeIs('projetos')) class="active" @endif>Projetos</a>
@if(Tools::routeIs('projetos*') && isset($categorias))
<div class="sub">
    @foreach($categorias as $cat)
    <a href="{{ route('projetos', $cat->slug) }}" @if(isset($categoria) && $cat->id == $categoria->id) class="active" @endif>{{ $cat->titulo }}</a>
    @endforeach
</div>
@endif
<a href="{{ route('clientes') }}" @if(Tools::routeIs('clientes')) class="active" @endif>Clientes</a>
<a href="{{ route('clipping') }}" @if(Tools::routeIs('clipping')) class="active" @endif>Clipping</a>
<a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>Contato</a>
@if($contato->facebook || $contato->instagram)
<div class="social">
    @if($contato->instagram)
    <a href="{{ $contato->instagram }}" target="_blank" class="instagram">instagram</a>
    @endif
    @if($contato->facebook)
    <a href="{{ $contato->facebook }}" target="_blank" class="facebook">facebook</a>
    @endif
</div>
@endif
