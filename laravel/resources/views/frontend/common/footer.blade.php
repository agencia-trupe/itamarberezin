    <footer>
        <div class="center">
            <p>
                © {{ date('Y') }} {{ config('app.name') }} &middot; Todos os direitos reservados.
                <span>|</span>
                <a href="http://www.trupe.net" target="_blank">Criação de Sites</a>:
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
