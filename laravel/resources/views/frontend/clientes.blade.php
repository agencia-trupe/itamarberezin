@extends('frontend.common.template')

@section('content')

    <div class="content clientes">
        <p>Alguns de nossos clientes</p>
        <div class="imagens">
            @foreach($clientes as $premio)
            <div class="imagem">
                <img src="{{ asset('assets/img/clientes/'.$premio->imagem) }}" alt="">
            </div>
            @endforeach
        </div>
    </div>

@endsection
