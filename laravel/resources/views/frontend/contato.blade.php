@extends('frontend.common.template')

@section('content')

    <div class="content contato">
        <h2>Contato</h2>

        <div class="informacoes">
            <p class="telefone">{{ $contato->telefone }}</p>
            <p class="endereco">{!! $contato->endereco !!}</p>
        </div>
        <form action="{{ route('contato.post') }}" method="POST">
            {!! csrf_field() !!}

            @if($errors->any())
                <div class="flash flash-erro">
                    @foreach($errors->all() as $error)
                    {!! $error !!}<br>
                    @endforeach
                </div>
            @endif

            @if(session('enviado'))
                <div class="flash flash-sucesso">
                    Mensagem enviada com sucesso!
                </div>
            @endif

            <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
            <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
            <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="telefone">
            <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
            <input type="submit" value="ENVIAR">
        </form>
    </div>

@endsection

@section('fullWidthContent')

    <div class="mapa">
        {!! $contato->google_maps !!}
    </div>

@endsection
