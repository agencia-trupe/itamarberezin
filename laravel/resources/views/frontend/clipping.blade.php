@extends('frontend.common.template')

@section('content')

    <div class="content clipping">
        <h2>Clipping</h2>
        <div class="clipping-thumbs">
            @foreach($clipping as $c)
                @if($c->video)
                <a href="{{ $c->video }}" class="clipping-video">
                    @if($c->capa)
                    <img src="{{ asset('assets/img/clipping/'.$c->capa) }}" alt="">
                    @else
                    <div class="titulo">
                        <div><span>{{ $c->titulo }}</span></div>
                    </div>
                    @endif
                    <div class="icone-video"></div>
                </a>
                @elseif($c->link)
                <a href="{{ Tools::parseLink($c->link) }}" class="clipping-link" target="_blank">
                    @if($c->capa)
                    <img src="{{ asset('assets/img/clipping/'.$c->capa) }}" alt="">
                    @else
                    <div class="titulo">
                        <div><span>{{ $c->titulo }}</span></div>
                    </div>
                    @endif
                </a>
                @else
                <a href="#" class="clipping-galeria" data-galeria="{{ $c->id }}">
                    @if($c->capa)
                    <img src="{{ asset('assets/img/clipping/'.$c->capa) }}" alt="">
                    @else
                    <div class="titulo">
                        <div><span>{{ $c->titulo }}</span></div>
                    </div>
                    @endif
                </a>
                @endif
            @endforeach
        </div>
    </div>

    <div style="display:none">
        @foreach($clipping as $c)
            @foreach($c->imagens as $imagem)
            <a href="{{ asset('assets/img/clipping/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria-{{ $c->id }}"></a>
            @endforeach
        @endforeach
    </div>

@endsection
