import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.banners').cycle({
    slides: '.banner'
});

$('.clipping-galeria').click(function(e) {
    e.preventDefault();

    var galeria = $(this).data('galeria');

    $('.fancybox[rel=galeria-' + galeria).first().trigger('click');
});

$('.fancybox').fancybox({
    padding: 0,
    helpers: {
        overlay: {
            css: {
                'background': 'rgba(0, 0, 0, .88)'
            }
        }
    }
});

$('.clipping-video').fancybox({
    padding: 0,
    type: 'iframe',
    width: 800,
    height: 450,
    aspectRatio: true,
    helpers: {
        overlay: {
            css: {
                'background': 'rgba(0, 0, 0, .88)'
            }
        }
    }
});
