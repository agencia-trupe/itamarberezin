<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Institucional extends Model
{
    protected $table = 'institucional';

    protected $guarded = ['id'];

    public static function upload_foto()
    {
        return CropImage::make('foto', [
            'width'  => 800,
            'height' => null,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 480,
            'height' => 277,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 480,
            'height' => 277,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 480,
            'height' => 277,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public static function upload_imagem_4()
    {
        return CropImage::make('imagem_4', [
            'width'  => 480,
            'height' => 277,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public static function upload_imagem_5()
    {
        return CropImage::make('imagem_5', [
            'width'  => 480,
            'height' => 277,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public static function upload_imagem_6()
    {
        return CropImage::make('imagem_6', [
            'width'  => 480,
            'height' => 277,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public static function upload_imagem_7()
    {
        return CropImage::make('imagem_7', [
            'width'  => 480,
            'height' => 277,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public static function upload_imagem_8()
    {
        return CropImage::make('imagem_8', [
            'width'  => 480,
            'height' => 277,
            'path'   => 'assets/img/institucional/'
        ]);
    }

}
