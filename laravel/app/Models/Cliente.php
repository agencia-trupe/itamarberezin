<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Cliente extends Model
{
    protected $table = 'clientes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 220,
            'height' => 140,
            'color'  => '#fff',
            'upsize' => true,
            'path'   => 'assets/img/clientes/'
        ]);
    }
}
