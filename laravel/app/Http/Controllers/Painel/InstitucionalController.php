<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\InstitucionalRequest;
use App\Http\Controllers\Controller;

use App\Models\Institucional;

class InstitucionalController extends Controller
{
    public function index()
    {
        $registro = Institucional::first();

        return view('painel.institucional.edit', compact('registro'));
    }

    public function update(InstitucionalRequest $request, Institucional $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['foto'])) $input['foto'] = Institucional::upload_foto();
            if (isset($input['imagem_1'])) $input['imagem_1'] = Institucional::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = Institucional::upload_imagem_2();
            if (isset($input['imagem_3'])) $input['imagem_3'] = Institucional::upload_imagem_3();
            if (isset($input['imagem_4'])) $input['imagem_4'] = Institucional::upload_imagem_4();
            if (isset($input['imagem_5'])) $input['imagem_5'] = Institucional::upload_imagem_5();
            if (isset($input['imagem_6'])) $input['imagem_6'] = Institucional::upload_imagem_6();
            if (isset($input['imagem_7'])) $input['imagem_7'] = Institucional::upload_imagem_7();
            if (isset($input['imagem_8'])) $input['imagem_8'] = Institucional::upload_imagem_8();

            $registro->update($input);

            return redirect()->route('painel.institucional.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
