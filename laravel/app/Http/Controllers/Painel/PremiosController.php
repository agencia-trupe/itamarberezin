<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PremiosRequest;
use App\Http\Controllers\Controller;

use App\Models\Premio;

class PremiosController extends Controller
{
    public function index()
    {
        $registros = Premio::ordenados()->get();

        return view('painel.premios.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.premios.create');
    }

    public function store(PremiosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Premio::upload_imagem();

            Premio::create($input);

            return redirect()->route('painel.premios.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Premio $registro)
    {
        return view('painel.premios.edit', compact('registro'));
    }

    public function update(PremiosRequest $request, Premio $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Premio::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.premios.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Premio $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.premios.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
