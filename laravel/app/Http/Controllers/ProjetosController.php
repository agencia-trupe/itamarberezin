<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\ProjetoCategoria;
use App\Models\Projeto;

class ProjetosController extends Controller
{
    public function __construct()
    {
        view()->share('categorias', ProjetoCategoria::ordenados()->get());
    }

    public function index(ProjetoCategoria $categoria)
    {
        if (! $categoria->exists) {
            $categoria = ProjetoCategoria::ordenados()->firstOrFail();
        }

        view()->share('categoria', $categoria);

        $projetos = $categoria->projetos;

        return view('frontend.projetos.index', compact('projetos'));
    }

    public function show(ProjetoCategoria $categoria, Projeto $projeto)
    {
        if ($projeto->categoria->id != $categoria->id) {
            abort('404');
        }

        view()->share('categoria', $categoria);

        return view('frontend.projetos.show', compact('projeto'));
    }
}
