<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Institucional;
use App\Models\Premio;

class PerfilController extends Controller
{
    public function institucional()
    {
        $institucional = Institucional::first();

        return view('frontend.institucional', compact('institucional'));
    }

    public function premios()
    {
        $premios = Premio::ordenados()->get();

        return view('frontend.premios', compact('premios'));
    }
}
