<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InstitucionalRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'foto' => 'image',
            'texto' => 'required',
            'imagem_1' => 'image',
            'imagem_2' => 'image',
            'imagem_3' => 'image',
            'imagem_4' => 'image',
            'imagem_5' => 'image',
            'imagem_6' => 'image',
            'imagem_7' => 'image',
            'imagem_8' => 'image',
        ];
    }
}
