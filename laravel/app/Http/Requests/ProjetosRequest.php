<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProjetosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'projetos_categoria_id' => 'required',
            'titulo' => 'required',
            'capa' => 'required|image',
            'cabecalho' => 'required|image',
            'local' => '',
            'projeto' => '',
            'area_do_terreno' => '',
            'area_construida' => '',
            'pavimentos' => '',
            'tipo' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
            $rules['cabecalho'] = 'image';
        }

        return $rules;
    }
}
