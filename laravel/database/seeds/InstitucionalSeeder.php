<?php

use Illuminate\Database\Seeder;

class InstitucionalSeeder extends Seeder
{
    public function run()
    {
        DB::table('institucional')->insert([
            'foto' => '',
            'texto' => '',
            'imagem_1' => '',
            'imagem_2' => '',
            'imagem_3' => '',
            'imagem_4' => '',
            'imagem_5' => '',
            'imagem_6' => '',
            'imagem_7' => '',
            'imagem_8' => '',
        ]);
    }
}
