<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitucionalTable extends Migration
{
    public function up()
    {
        Schema::create('institucional', function (Blueprint $table) {
            $table->increments('id');
            $table->string('foto');
            $table->text('texto');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->string('imagem_4');
            $table->string('imagem_5');
            $table->string('imagem_6');
            $table->string('imagem_7');
            $table->string('imagem_8');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('institucional');
    }
}
