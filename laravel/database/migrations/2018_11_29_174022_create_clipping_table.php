<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClippingTable extends Migration
{
    public function up()
    {
        Schema::create('clipping', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('capa');
            $table->string('link');
            $table->string('video');
            $table->timestamps();
        });

        Schema::create('clipping_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clipping_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('clipping_id')->references('id')->on('clipping')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('clipping_imagens');
        Schema::drop('clipping');
    }
}
